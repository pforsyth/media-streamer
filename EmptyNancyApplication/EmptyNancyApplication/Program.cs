﻿using Nancy;
using Nancy.Responses;

namespace EmptyNancyApplication
{
    using System;
    using Nancy.Hosting.Self;

    class Program
    {
        static void Main(string[] args)
        {
            var uri =
                new Uri("http://localhost:3579");
            var hostConfig = new Nancy.Hosting.Self.HostConfiguration();
            hostConfig.AllowChunkedEncoding = false;
            GenericFileResponse.SafePaths.Add("d:\\");
            hostConfig.UnhandledExceptionCallback = exception =>
                {
                    Console.WriteLine(string.Format("EXCEPTION - {0}", exception));
                };
            using (var host = new NancyHost(hostConfig, uri))
            {
                host.Start();
                Console.WriteLine("Your application is running on " + uri);
                Console.WriteLine("Press any [Enter] to close the host.");
                Console.ReadLine();
            }
        }
    }
}
