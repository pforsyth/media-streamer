﻿using System;
using System.IO;
using Nancy;
using Nancy.Responses;

namespace EmptyNancyApplication.Modules
{
    public class IndexModule : NancyModule
    {
        public IndexModule()
        {
            Get["/"] = parameters => OnFunc(parameters);
            Get["/Stream/PlaySingleTrack/{id}"] = parameters => OnPlay(parameters);
        }

        private dynamic OnPlay(DynamicDictionary parameters)
        {
            var response = new StreamResponse(Source, "audio/mpeg");
            var stream = new FileStream("d:\\02. Chicane - Saltwater.mp3", FileMode.Open, FileAccess.Read,
                                        FileShare.Read);
            response.Headers.Add("Content-Range", string.Format("0-{0} / {1}", stream.Length - 1, stream.Length));
            stream.Close();
            return response;
        }

        private Stream Source()
        {
            return new FileStream("d:\\02. Chicane - Saltwater.mp3", FileMode.Open, FileAccess.Read, FileShare.Read);
        }

        private dynamic OnFunc(dynamic parameters)
        {
            return "Hello world";
        }

    }
}
