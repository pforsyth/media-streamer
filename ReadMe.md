# Media Streamer
----------------
The intent of this product is to deliver a streaming solution in .Net in three parts.

1. An agent installed on the home pc which will index music on the home machine available for streaming remotely. The agent will allow for commands such as stream, managing playlists, rating music as you play etc. There should also be a simple console on the home machine to allow configuration (proxy details, host port number, authentication details, indexable folders etc).

2. A web UI. Basically to allow you to sign up and play/manage your music collection online. Service calls are redirected to the home agent to retrieve music, create playlists, play music etc. The remote server simply ties these two together in terms of being able to contact the agent from anywhere in the world.

3. There may be an API which will allow third party clients (mobile if required).

# Technology Stack
------------------
Agent         : Windows Service, in-host ravendb, in-host Mvc WebApi for external communication.
Agent Console : Undecided, probably winforms or WPF.
Web Server    : MVC4 + WebApi (to allow the agent to communicate, mainly for connection ping and to authorize intial connection etc.

# Components
------------------
Agent
-----
File monitor      : Monitor indexed locations to see if any music has been added or removed. Initially on a timer, either monitor always or on a timer.
Indexer           : Can start by just indexing filenames and paths to files, but expand later to index titles, artists, album names etc.
Search            : Perform queries on the index to find tracks. Evolves in line with indexer
Streaming API     : To stream (remember byte range requests are the whole reason for doing this as subsonic doesn't appear to do it very well at all)
Playlist mgmt API : Allow the user to create playlists, store current playing state etc. Indexer might auto generate playlists based on .m3u files as well.
Configuration API : Configuration of Agent settings. E.g. monitored file paths, index timeout, associate with account (online).

Web
---
User management   : Create user, allow user to login.
Link to agent     : Perform from agent, once user is registered, they need to sign in with their agent in order to associate their account to the installed agent. The agent should also periodically ping the server to ensure details are up to date (how often? Only really needed to ensure IP etc is correct on the server).
Redirect to agent : Streaming/playlists etc should come from the agent so the web UI needs to forward all requests to that client - as well as picking up client configuration changes (e.g. change of IP).


#Features
#--------
1. Listing of media by Song title/Artist, Folder Structure or by playlist.
