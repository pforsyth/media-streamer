﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUI.Models
{
    public class MusicItem
    {
        public string Id { get; set; }
        public string Title { get; set; }
    }
}