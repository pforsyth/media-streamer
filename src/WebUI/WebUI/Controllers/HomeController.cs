﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;
using WebUI.Models;

namespace WebUI.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            var files = new List<MusicItem>();
            files.Add(new MusicItem() {Id = Guid.NewGuid().ToString(), Title = "Dummy track"});
            return View(files);
        }

    }
}
