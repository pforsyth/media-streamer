﻿using System;
using System.Web.Mvc;

namespace WebUI.Controllers
{
    public class StreamController : Controller
    {
        public ActionResult PlaySingleTrack(Guid id)
        {
            return new RedirectResult(string.Format("http://localhost:50231/api/stream/{0}", id));
        }
    }
}
