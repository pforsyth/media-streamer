﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using MediaStreamer.MediaAgent.Index;
using Raven.Client;

namespace MediaStreamer.MediaAgent.WebAPI
{
    public class StreamController : ApiController
    {
        private static readonly MediaTypeHeaderValue _mediaType = MediaTypeHeaderValue.Parse("audio/mpeg");
        public static IDocumentStore _documentStore;
        public HttpResponseMessage Get(Guid id)
        {
            var repo = new MusicRepository(_documentStore);
            var item = repo.GetItem(id);
            var memStream = new FileStream("d:\\02. Chicane - Saltwater.mp3", FileMode.Open, FileAccess.Read);
            if (Request.Headers.Range != null)
            {
                try
                {
                    var byteStream = new ByteRangeStreamContent(memStream, Request.Headers.Range, _mediaType);
                    var response = Request.CreateResponse(HttpStatusCode.PartialContent);
                    response.Content = byteStream;
                    return response;
                }
                catch (InvalidByteRangeException invalidByteRangeException)
                {
                    return Request.CreateErrorResponse(invalidByteRangeException);
                }
            }
            var fullResponse = Request.CreateResponse(HttpStatusCode.OK);
            fullResponse.Content = new StreamContent(memStream);
            fullResponse.Content.Headers.ContentType = _mediaType;
            return fullResponse;
        }
    }
}