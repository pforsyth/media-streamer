﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using MediaStreamer.MediaAgent.Index;
using Raven.Client;

namespace MediaStreamer.MediaAgent.WebAPI
{
    public class QueryController : ApiController
    {
        public static IDocumentStore _documentStore;
        public IEnumerable<MusicItem> GetAllMusicFiles()
        {
            using (var session = _documentStore.OpenSession())
            {
                var items = session.Query<MusicFile>().ToList();
                var itemsToReturn = new List<MusicItem>();
                foreach (var item in items)
                {
                    var itemToAdd = new MusicItem
                                        {
                                            Id = item.Id,
                                            Title = string.IsNullOrEmpty(item.Title) ? "No Title" : item.Title
                                        };
                    itemsToReturn.Add(itemToAdd);
                }
                return itemsToReturn;
            }
        }
    }

    public class MusicItem
    {
        public string Id { get; set; }
        public string Title { get; set; }
    }
}