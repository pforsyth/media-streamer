﻿using System;
using System.Web.Http;
using System.Web.Http.SelfHost;
using Raven.Client;

namespace MediaStreamer.MediaAgent.WebAPI
{
    public class ApiHost
    {
        private static readonly Uri _baseAddress = new Uri("http://localhost:50231/");
        private readonly HttpSelfHostServer server;
        private readonly IDocumentStore _documentStore;

        public ApiHost(IDocumentStore document)
        {
            var config = new HttpSelfHostConfiguration(_baseAddress);
            config.Routes.MapHttpRoute("API Default", "api/{controller}/{id}", new { id = RouteParameter.Optional });
            server = new HttpSelfHostServer(config);
            _documentStore = document;
            StreamController._documentStore = _documentStore;
            QueryController._documentStore = _documentStore;
        }

        public void Start()
        {
            server.OpenAsync().Wait();
        }
       
        public void Stop()
        {
            server.CloseAsync();
        }
    }
}
