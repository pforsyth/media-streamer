﻿using System;
using System.Web.Http;

namespace MediaStreamer.MediaAgent.WebAPI
{
    public class PlaylistController : ApiController
    {
        public void GetCurrentlyPlayingTrackId()
        {   
        }

        public void UpdateCurrentlyPlaying(Guid songId)
        {
        }

        public void GetCurrentPlaylist()
        {  
            // Return ordered current playlist.
        }

        public void AddSongToCurrentlyPlaying()
        {
        }

        public void RemoveSongFromCurrentlyPlaying()
        {
        }
    }
}
