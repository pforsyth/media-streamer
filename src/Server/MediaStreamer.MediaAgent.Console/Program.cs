﻿using System;
using System.Diagnostics;
using System.Web.Http.SelfHost;
using MediaStreamer.MediaAgent.Index;
using MediaStreamer.MediaAgent.WebAPI;
//using Raven.Client.Embedded;

namespace MediaStreamer.MediaAgent.Console
{
    class Program
    {
        
        public static Stopwatch _sw = new Stopwatch();


        static void Main(string[] args)
        {
            
            try
            {
                var fileIndexer = new FileIndexer(new MusicRepository(null));

                FileProvider provider = new FileProvider(@"D:\\personal\\Music");
                _sw.Start();
                int itemCounter = 0;
                provider.StartProcessing(s =>
                                             {
                                                 fileIndexer.IndexFile(s);
                                                 itemCounter++;
                                                 System.Console.WriteLine("Queued {0} in {1}", itemCounter, _sw.Elapsed);
                                             });
                var host = new ApiHost(null);
                host.Start();
                System.Console.WriteLine("Hit ENTER to exit...");
                System.Console.ReadLine();
            }
            catch (Exception e)
            {
                System.Console.WriteLine("Could not start server: {0}", e.GetBaseException().Message);
                System.Console.WriteLine("Hit ENTER to exit...");
                System.Console.ReadLine();
            }
            finally
            {
                
            }
        }
    }
}