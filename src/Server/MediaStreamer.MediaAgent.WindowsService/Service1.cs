﻿using System.ServiceProcess;
using Raven.Client.Embedded;

namespace MediaStreamer.MediaAgent.WindowsService
{
    public partial class Service1 : ServiceBase
    {
        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            // Start task 
            // Open database
            var docStore = new EmbeddableDocumentStore();
            docStore.DataDirectory = @"D:\\test";
            docStore.UseEmbeddedHttpServer = true;
            //docStore.RunInMemory = true;
            docStore.Configuration.Port = 9000;
            docStore.Initialize();

            // Get credentials
            // Verify with website
            // Start "ping" service, allow pings to go back and forward and keep configuration information up to date.
            // Start streamer service
            // Start indexer
            // // Check for last index time
            // // Perform index if greater than 1 day ago.
        }

        protected override void OnStop()
        {
        }
    }
}