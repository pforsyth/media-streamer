﻿using System;

namespace MediaStreamer.MediaAgent.Index
{
    public class MusicFile
    {
        public MusicFile(Guid newGuid)
        {
            Id = newGuid.ToString();
        }

        public string Id { get; protected set; }
        public string Title { get; set; }
        public string Artist { get; set; }
        public TimeSpan Duration { get; set; }
        public string FullPath { get; set; }
        public long FileSize { get; set; }
        public DateTime LastModified { get; set; }
        public string MimeType { get; set; }
    }
}
