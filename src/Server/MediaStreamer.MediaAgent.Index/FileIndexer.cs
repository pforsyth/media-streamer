﻿using System;
using System.IO;

namespace MediaStreamer.MediaAgent.Index
{
    public class FileIndexer
    {
        private IIndexStore _indexStore;
        public FileIndexer(IIndexStore indexStore)
        {
            _indexStore = indexStore;
        }

        public void IndexFile(string path)
        {
            var musicFile = new MusicFile(Guid.NewGuid());
            FileInfo fi = new FileInfo(path);
            // Check for the path
            // then check the modified date + filesize
            musicFile.LastModified = fi.LastWriteTimeUtc;
            musicFile.FileSize = fi.Length;
            musicFile.FullPath = path;
            // If modified date or filesize have changed, re-index, otherwise skip.
            var file = TagLib.File.Create(path);
            musicFile.Title = file.Tag.Title;
            musicFile.Duration = file.Properties.Duration;
            IndexFile(musicFile);
        }

        private void IndexFile(MusicFile musicFile)
        {
            _indexStore.StoreIndexEntry(musicFile);
        }
    }
}
