﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace MediaStreamer.MediaAgent.Index
{
    public class FileProvider : IFileProvider
    {
        private string _path;
        private Action<string> _fileHandler;
        public FileProvider(string path)
        {
            _path = path;
        }

        public void StartProcessing(Action<string> fileHandler)
        {
            _fileHandler = fileHandler;
            GetFilesFromDir(_path);
        }

        private void GetFilesFromDir(string path)
        {
            List<string> files;
            try
            {
                var dir = new DirectoryInfo(path);
                files = dir.GetFiles("*.mp3").Select(p => p.FullName).ToList();
                ProcessFiles(files);
                var dirs = dir.GetDirectories();
                if (!dirs.Any()) return;
                foreach (var childDir in dirs)
                {
                    GetFilesFromDir(childDir.FullName);
                }
            }
            catch (PathTooLongException)
            {
                // Long paths are a problem in general in windows systems
            }
            catch (UnauthorizedAccessException)
            {
                // The user is not permitted to access this location
            }
            catch (DirectoryNotFoundException)
            {
                // the actual folder doesn't exist
            }
            catch (IOException)
            {
                // Could not access
            }
            catch (Exception e)
            {
                _exceptions.Add(new CaughtException(e, path));
            }
        }

        private void ProcessFiles(List<string> files)
        {
            files.ForEach(HandleFile);
        }
        
        private void HandleFile(string file)
        {
            try
            {
                _fileHandler(file);
            }
            catch (Exception exception)
            {
                _exceptions.Add(new CaughtException(exception, file));

            }
        }
        private List<CaughtException> _exceptions = new List<CaughtException>();

        public List<CaughtException> Exceptions { get { return _exceptions; } }
    }

    public class CaughtException
    {
        public Exception Exception { get; private set; }
        public string Path { get; private set; }
        public CaughtException(Exception exception, string path)
        {
            Exception = exception;
            Path = path;
        }
    }
}
