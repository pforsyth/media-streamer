﻿
using System;
using System.Collections.Generic;
using System.IO;

namespace MediaStreamer.MediaAgent.Index
{
    public interface IFileProvider
    {
        void StartProcessing(Action<string> fileHandler);
    }
}
