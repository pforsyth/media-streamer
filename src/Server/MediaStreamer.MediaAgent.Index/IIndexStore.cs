﻿using System.Linq;

namespace MediaStreamer.MediaAgent.Index
{
    public interface IIndexStore
    {
        void StoreIndexEntry(MusicFile index);
        IQueryable<MusicFile> Queryable();
    }
}