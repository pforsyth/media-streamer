﻿using System;
using System.Linq;
using Raven.Client;

namespace MediaStreamer.MediaAgent.Index
{
    public class MusicRepository : IIndexStore
    {
        private readonly IDocumentStore _documentStore;
        public MusicRepository(IDocumentStore dbPath)
        {
            _documentStore = dbPath;
        }

        public void StoreIndexEntry(MusicFile index)
        {
            var session = _documentStore.OpenSession();
            session.Store(index);
            session.SaveChanges();
        }

        public IQueryable<MusicFile> Queryable()
        {
            using (var session = _documentStore.OpenSession())
            {
                return session.Query<MusicFile>();
            }
        }

        public MusicFile GetItem(Guid id)
        {
            return null;
            using (var session = _documentStore.OpenSession())
            {
                return session.Load<MusicFile>(id.ToString());
            }
        }
    }
}
